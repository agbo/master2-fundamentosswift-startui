//
//  GreenViewController.swift
//  startupUI
//
//  Created by Fernando Rodríguez Romero on 25/11/15.
//  Copyright © 2015 KeepCoding. All rights reserved.
//

import UIKit

class GreenViewController: UIViewController {

    //MARK: - Actions
    @IBAction func tappedOnBlue(sender: AnyObject) {
        print("Soy un IBAction!")
    }
    
    //MARK: - Unwind
    @IBAction func aCascarla(segue: UIStoryboardSegue){
        
        print("A cascarla! He hecho mi primer unwind segue")
    }
    
    //MARK: - Scene Transitions
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        // Entoavía no se ha mostrado la vista
        // Averiguamos qué segue es, ya que podría haber muchas
        
        if segue.identifier == "displayBlue" {
            // Tenemos que actualizar el azul
            // antes que aparezca para que muestre
            // su modelo
            let destVC = segue.destinationViewController as? BlueViewController
            
            destVC?.name = "Azul Cobalto"
            
            
        }else{
            print("Segue desconocida")
        }
        
    }
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
